import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { add_user , } from "../Config/Actions";
import { useNavigate } from "react-router-dom";
function AddUser() {
  const dispatch = useDispatch();
  const [nom, setNom] = useState("");
  const [prenom, setPrenom] = useState("");
  const [gmail, setGmail] = useState("");
  const navigate=useNavigate  ()
  function handelClick(e) {
    e.preventDefault()
    dispatch(add_user(nom, prenom, gmail));
    navigate('/')
  }
  return (
    <div className="container">
      <h1 className="text-center mt-5">Ajouter un utilisateur</h1>
      <form action="">
        <div className="form-group">
          <label
            className="col-form-label col-form-label-lg mt-4"
            htmlFor="inputLarge"
          >
            Nom :
          </label>
          <input
            className="form-control form-control-lg"
            type="text"
            id="inputLarge"
            name="nom"
            onChange={(e) => setNom(e.target.value)}
          />
          <label
            className="col-form-label col-form-label-lg mt-4"
            htmlFor="inputLarge"
          >
            Prénom :
          </label>
          <input
            className="form-control form-control-lg"
            type="text"
            id="inputLarge"
            name="prenom"
            onChange={(e) => setPrenom(e.target.value)}
          />
          <label className="form-label mt-4">Gmail</label>
          <div className="form-floating mb-3">
            <input
              type="email"
              className="form-control"
              id="floatingInput"
              name="gmail"
              onChange={(e) => setGmail(e.target.value)}
            />
            <label htmlFor="floatingInput">name@example.com</label>
          </div>
          <button className="btn btn-primary float-end" onClick={handelClick}>
            Ajouter
          </button>
        </div>
      </form>
    </div>
  );
}

export default AddUser;
