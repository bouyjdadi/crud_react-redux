import React from "react";

function Footer() {
  return (
    <div className="mb-4 text-center">
      <section>
        Copyright &copy; 2023 Kawtar_Bouyjdadi 😊.All rights reserved.
      </section>
    </div>
  );
}

export default Footer;
