let id = 0;
let intialstate = {
  usersTables: [],
  usersFilter: [],
};
export const users = (state = intialstate, action) => {
  switch (action.type) {
    case "ADD_ARTICLE":
      return {
        ...state,
         usersTables:[
        ...state.usersTables,
        {
          id: ++id,
          nom: action.payload.nom,
          prenom: action.payload.prenom,
          gmail: action.payload.gmail,
        }
      ]
      }
     
    case "DELETE_ARTICLE":
      return {...state,
        usersTables:[...state.usersTables.filter((item) => item.id !== action.payload.id)]}
    case "UPDATE_ARTICLE":
      return {...state,
        usersTables:[...state.usersTables.map((obj) => {
        return obj.id === parseInt(action.payload.id) ? action.payload : obj
      })]}
    case "FILTER_ARTICLE":
      return {
        ...state,
        usersFilter:[...state.usersTables.filter((user) => user.nom === action.payload.nom)],
      };
  
    default:
      return state;
  }
};
 
